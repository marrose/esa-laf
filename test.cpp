#include <algorithm>
#include <cassert>
#include <iostream>
#include <map>
#include "laf.h"

// computes max LAF weight on a given set of strings
int weight(const std::vector<std::vector<int>>& ss) {
  std::map<std::vector<int>, int> counts;

  for (const auto& s : ss) {
    std::map<std::vector<int>, std::pair<int, int>> posAndCount;
    // positions : substring -> (last position, non-overlapping instance count)
    for (size_t i = 0; i < s.size(); ++i) {
      std::vector<int> t;  // substring
      for (size_t j = i; j < s.size(); ++j) {
        t.push_back(s[j]);
        if (!posAndCount.count(t)) {
          posAndCount[t] = {i, 1};
        } else if (posAndCount[t].first + t.size() <= i) {
          posAndCount[t].second++;
          posAndCount[t].first = i;
        }
      }
    }
    // update global counts
    for (const auto& pp : posAndCount) {
      counts[pp.first] += pp.second.second;
    }
  }

  int w = 0;
  for (auto&& p : counts) {
    if (p.second > 1) {
      w = std::max(w, ((int)p.first.size() - 1) * p.second);
    }
  }
  return w;
}

void outputString(const std::vector<int>& t) {
  for (int c : t) {
    if (c > TERMINAL_OFFSET)
      std::cout << '*' << int(c - TERMINAL_OFFSET) << '*';
    else
      std::cout << char(c);
  }
  std::cout << "\n";
}

void test(const std::string& s) {
  std::vector<std::vector<int>> output = laf(s);
  std::vector<int> compressedText = output[0];

  auto rules = output;
  rules.erase(rules.begin());

  std::vector<std::vector<int>> ts;
  // ts: current set of strings (i.e. input + rules)
  {  // add input string
    std::vector<int> inputStr;
    for (char c : s) inputStr.push_back(c);
    ts.push_back(inputStr);
  }

  // to verify if rules were optimal in their respective iteration we have
  // to revert them to their uncompressed version (i.e. unwind all non-terminals
  // that came later).
  std::vector<std::vector<int>> unwindedRules;
  for (size_t i = 0; i < (int)rules.size(); ++i) {
    std::vector<int> unwindedRule = rules[i];

    // unwinding algorithm is super inefficient..but it's fine for small strings
    while (true) {
      bool done = true;
      std::vector<int> newUnwindedRule;
      for (int c : unwindedRule) {
        if (c > TERMINAL_OFFSET && c - TERMINAL_OFFSET > i + 1) {
          int r = c - TERMINAL_OFFSET;
          newUnwindedRule.insert(newUnwindedRule.end(), rules[r - 1].begin(),
                                 rules[r - 1].end());
          done = false;
        } else {
          newUnwindedRule.push_back(c);
        }
      }
      unwindedRule = std::move(newUnwindedRule);
      if (done) break;
    }
    unwindedRules.push_back(std::move(unwindedRule));
  }

  // now we verify rules one-by-one to confirm they were indeed optimal in the
  // given situation
  for (size_t ruleIdx = 1; ruleIdx <= unwindedRules.size(); ++ruleIdx) {
    auto& rule = unwindedRules[ruleIdx - 1];

    assert(rule.size() > 1);

    std::vector<std::vector<size_t>> positions;
    int totalPositionsCount = 0;
    for (auto& t : ts) {
      std::vector<size_t> pos;
      for (size_t i = 0; i + rule.size() <= t.size(); ++i) {
        if (std::equal(rule.begin(), rule.end(), t.begin() + i)) {
          if (pos.size() == 0 || pos.back() + rule.size() <= i) {
            pos.push_back(i);
          }
        }
      }
      positions.push_back(pos);
      totalPositionsCount += pos.size();
    }

    int w = totalPositionsCount * ((int)rule.size() - 1);

    // verify that optimal weight is equal to w
    if (w != weight(ts)) {
      // if the following assert fails, we have a bug in the tester
      assert(w < weight(ts));

      std::cout << "\n\nFAIL!\n";
      std::cout << "Input: " << s << "\n\n";

      std::cout << "In iteration " << ruleIdx
                << " algorithm finds a string of weight " << w
                << " while there is one of weight " << weight(ts) << ".\n\n";
      std::cout << "String that we find: ";
      outputString(rule);
      std::cout << "\n";

      std::cout << "Rules up to this one (in their unwinded form):\n";
      for (int i = 1; i < ruleIdx; ++i) {
        outputString(unwindedRules[i - 1]);
      }

      std::cout << "\nCurrent set of strings:\n";
      for (auto& t : ts) {
        outputString(t);
      }

      exit(1);
    }

    assert(totalPositionsCount > 1);

    // replace instances of the new rule
    for (size_t ti = 0; ti < ts.size(); ++ti) {
      std::vector<int>& t = ts[ti];
      const auto& pos = positions[ti];
      std::vector<int> nt;

      for (size_t i = 0; i < t.size(); ++i) {
        if (std::binary_search(pos.begin(), pos.end(), i)) {
          nt.push_back(TERMINAL_OFFSET + ruleIdx);
          i += rule.size() - 1;
        } else {
          nt.push_back(t[i]);
        }
      }

      t = nt;
    }
    // add new rule to the set of strings
    ts.push_back(rule);
  }

  // verify that we are indeed done
  if (weight(ts) > 1) {
    std::cout << "\n\nFAIL!\n";
    std::cout << "Input: " << s << "\n\n";

    std::cout
        << "Our algorithm terminated but there is more rules to be added.\n";

    std::cout << "Rules (in their unwinded form):\n";
    for (int i = 1; i <= unwindedRules.size(); ++i) {
      outputString(unwindedRules[i - 1]);
    }

    std::cout << "Set of strings at the end:\n";
    for (auto& t : ts) {
      outputString(t);
    }

    exit(1);
  }
}

void testAllBinaryStrings(int maxLength) {
  for (int n = 1; n <= maxLength; ++n) {
    std::cout << "Testing binary strings n = " << n << "\n";
    for (int s = 0; s < (1 << n); ++s) {
      std::string a;
      for (int i = 0; i < n; ++i) {
        int ms = (s >> i) & 1;
        a.push_back('a' + ms);
      }
      test(a);
    }
  }
  std::cout << "Done! All tests were successful\n";
}

void testRandomStrings(int n, int k) {
  srand(123452289);

  int cnt = 0;

  while (true) {
    std::string a;
    for (int i = 0; i < n; ++i) {
      a.push_back(rand() % k + 'a');
    }
    test(a);
    cnt++;

    if (cnt % 1000 == 0) {
      std::cout << "Number of random strings tested so far: " << cnt << "\n";
    }
  }
}

// Function "test" can be used to perform a test on an arbitrary string.
// Given are two input generators ("testRandomStrings" and
// "testAllBinaryStrings"). To extend with your own generator simply generate
// strings and invoke "test" function on each of them.
// First one that fails the test will terminate the program.
int main(int argc, char** argv) {
  if (argc != 3) {
    std::cout <<
    "Usage: ./test n k\n\n"
    "Performs algorithm optimality test on a series of strings.\n"
    "First one that fails the test will terminate the program and provide the user\n"
    "with a helpful debuging output.\n\n"
    "Tested are random strings of length n over the alphabet consisting\n"
    "of first k lowercase English letters.\n\n"
    "Special value of k = -1 is used for an exhaustive test over all the possible\n"
    "strings of length at most n and alphabet consisting of only letters a and b.\n";
    return 1;
  }

  int n = std::stoi(argv[1]);
  int k = std::stoi(argv[2]);

  test("abcdxabcdybcxcdefycdefxcd");  // sample from the paper

  if (k == -1) {
    testAllBinaryStrings(n);
  } else {
    testRandomStrings(n, k);
  }
  return 0;
}