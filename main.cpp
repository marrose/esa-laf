#include "laf.h"
#include <fstream>
#include <iostream>

using namespace std;


std::string read(std::string infile) {
  // Read input from file
  ifstream in(infile, ios::ate);
  if (!in.is_open()) {
    printf("Cannot open %s.\n", infile.c_str());
    exit(1);
  }

  int size = in.tellg();
  in.seekg(0, ios::beg);
  char* memblock = new char[size + 1];
  in.read(memblock, size - 1);  // skip newline
  in.close();

  std::string ret(memblock, size - 1);
  delete[] memblock;
  return ret;
}


int main(int argc, char** argv) {
  if (argc != 3) {
    puts(
        "To perform the largest area first parsing for grammar compression "
        "please run the program with two input strings:\n\n"
        "  1. Name of the input file\n"
        "  2. Name of the output file\n"
        "The output file will consist of a list of rules separated by new "
        "lines, and a grammar "
        "compressed version of the input file. In both the rule "
        "representation and the final output the "
        "rules will be represented as *N* where N is the ordinal number of a "
        "rule.\n\n");
    return 1;
  }

  string text = read(argv[1]);
  ofstream out;
  out.open(argv[2]);

  vector<vector<int>> output = laf(text);

  { // Print rules
    int rl_count = 0, ch_count = 0;
    for (int i = 1; i < (int)output.size(); i++) {
      out << 'R' << i << ": ";
      for (int ch: output[i]) {
        if (ch > TERMINAL_OFFSET) {
          out << '*' << to_string(ch-TERMINAL_OFFSET) << '*';
          rl_count++;
        } else {
          out << char(ch);
          ch_count++;
        }
      }
      out << endl;
    }

    cout << "Original string length in terminals: " << text.size() << endl;
    cout << "Number of rules: " << (int)output.size() - 1 << endl;
    cout << "Total rule length:  " << rl_count << " rule symbols & " << ch_count
        << " terminals" << endl;
  }

  { // Print compressed input text
    int rl_count = 0, ch_count = 0;
    out << "GRAMMAR COMPRESSED INPUT: " << endl;
    for (int ch: output[0]) {
      if (ch > TERMINAL_OFFSET) {
        out << '*' << to_string(ch-TERMINAL_OFFSET) << '*';
        rl_count++;
      } else {
        out << char(ch);
        ch_count++;
      }
    }
    out << endl;
  
    cout << "Output string length:  " << rl_count << " rule symbols & "
         << ch_count << " terminals" << endl;
  }

  return 0;
}
