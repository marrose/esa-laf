#include <cstring>

// Simple 12-byte vector
#define SMALL_BUFFER 4U

template<typename T>
struct __attribute__ ((__packed__)) Vector {
  // constexpr static uint32_t SMALL_BUFFER = 8;

  T* storage_;
  uint32_t size_;

public:
  Vector() : storage_(nullptr), size_(0) {};

  void push_back(const T& t) {
    if (((size_ & (size_-1)) == 0 && size_ >= SMALL_BUFFER) || size_ == 0) {
      uint32_t alloc = std::max(size_ * 2, SMALL_BUFFER);
      T* newStorage = new T[alloc];
      memcpy(newStorage, storage_, size_ * sizeof(T));
      delete[] storage_;
      storage_ = newStorage;
    }
    storage_[size_++] = t;
  }

  uint32_t size() const { return size_; }
  T& operator[](int i) { return storage_[i]; }
  const T& operator[](int i) const { return storage_[i]; }
};

