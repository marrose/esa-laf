/*
ESA LAF

Copyright © 2016 Ivan Katanic
Edited 2019 by Martin Rosenzweig

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "laf.h"
#include <algorithm>
#include <cassert>
#include <iostream>
#include <ctime>
#include <iomanip>
#include <stack>
#include <sstream>
#include <cstdarg>
#include <vector>
#include <cstring>
#include "vector.h"

using namespace std;

#define likely(x)      __builtin_expect(!!(x), 1)
#define unlikely(x)    __builtin_expect(!!(x), 0)

#include "sais.cpp"

#define LOG   1
#define DEBUG 0
#define ASSERT(x) if (DEBUG) assert(x);

#define SetFlag(s, x) (s) |= (x)
#define UnsetFlag(s, x) (s) &= ~(x)
#define TestFlag(s, x) ((s) & (x))

namespace {

void log(const char* format, ...) {
  if (!LOG) return;

  auto t = std::time(nullptr);
  auto tm = *std::localtime(&t);
  auto ts = std::put_time(&tm, "%Y-%m-%d %H:%M:%S");
  std::stringstream ss;
  ss << ts;
  printf("[%s]: ", ss.str().c_str()); 
  va_list argptr;
  va_start(argptr, format);
  vprintf(format, argptr);
  va_end(argptr);
  printf("\n");
  fflush(stdout);
}

enum Flags {
  Active = 1 << 0,
  Dead = 1 << 1,
  SamePositionsAsParent = 1 << 2,
  WeightChanged = 1 << 3,
};

using RuleId = uint32_t;
// ruleId == 0 denotes no rule

struct __attribute__ ((__packed__)) Character { 
  int ch;
  int prev;
  int next;
  RuleId ruleId; // rule character at this position
  RuleId killedBy; // rule that removed this input character
  Vector<RuleId> ruleIds; // all applied rules at this position
};
static_assert(sizeof(Character) == 32, "");

struct Rule {
  int len;
  int first;

  Rule(int len, int first): len(len), first(first) {}
};


class __attribute__ ((__packed__)) Position {
  public:
 
  int p;
  unsigned char flags;

  Position(int _p = 0) : p(_p), flags(0) {}
  
  int pos() const { return p; }
};
static_assert(sizeof(Position) == 5, "");

string getSubstring(int a, int b);

struct Node {  
  Position* positions;  // suffixes in this node's subtree; sorted by starting position
  uint32_t positionsCnt;

  int len;  // original lcp (i.e. interval's lcp over the original string)
  int lcp;  // decreases over time, interval's lcp over the current string
  int parent;
  int cntActive;
  RuleId lastRuleUpdate : 28;
  char flags : 4;

  int weight() { return cntActive * (lcp - 1); }
 
  void setCntActive(int val) {
    if (val == cntActive) return;
    cntActive = val;
    SetFlag(flags, WeightChanged);
  }
  
  void setLcp(int val) {
    if (val == lcp) return;
    lcp = val;
    SetFlag(flags, WeightChanged);
  }

  Node(): 
    positions(nullptr), 
    positionsCnt(0),
    len(0),
    lcp(0),
    parent(0),
    cntActive(0),
    lastRuleUpdate(0),
    flags(0) {}
};
static_assert(sizeof(Node) == 32, "");


// ----------------------- Memory ----------------------------
// Total:
//        LEN x 84 
//        LEN x alpha x 9 
//        NUM_RULES x 32
//        MAX_WEIGHT x 24

// linked-list representing current state of the string
vector<Character> s; // LEN x 32 

// rules
vector<Rule> rules; // NUM_RULES x 8
vector<string> pravila; // NUM_RULES x 24

vector<Node> nodes; // LEN x (32 + 9alpha)

vector<int> SA, lcp; // LEN x 8

// leaf id for every suffix
vector<int> TerminalNode; // LEN x 4  

// Priority queue of LCP tree nodes, indexed by the weight
vector<vector<int>> pq; // MAX_WEIGHT x 24

// all inverse position laid out
vector<int> invPosStorage; // LEN x alpha x 4
// Inverse positions matrix
vector<int*> InversePositions; // LEN x 8

// input
string text; // LEN
int len;
// ------------------------------------------------------

inline RuleId longestRuleAtPos(int pos, int maxLen) {
  // Function that returns the longest rule shorter than len that starts on
  // position pos
  RuleId ret = 0;
  for (uint32_t i = 0; i < s[pos].ruleIds.size(); ++i) {
    RuleId rule = s[pos].ruleIds[i];
    if (rules[rule].len < maxLen && rules[rule].len > rules[ret].len)
      ret = rule;
  }
  
  return ret;
}

void initSA() {
  log("initSA start");
  unsigned char* tmp = new unsigned char[len + 1];
  for (int i = 0; i < len; ++i) tmp[i] = text[i];
  tmp[len] = 0;

  SA.resize(len + 1);
  SA_IS(tmp, &SA[0], len + 1, 255, 1);
  ASSERT(SA[0] == len);

  vector<int> invSA(len);
  for (int i = 0; i < len; ++i) {
    SA[i] = SA[i + 1];
    invSA[SA[i]] = i;
  }
  SA.pop_back();

  lcp.resize(len);
  lcp[0] = 0;
  int h = 0;
  for (int i = 0; i < len; ++i) {
    if (invSA[i] > 0) {
      int j = SA[invSA[i] - 1];
      while (i + h < len && j + h < len && tmp[i + h] == tmp[j + h]) h++;
      lcp[invSA[i]] = h;
      if (h > 0) --h;
    }
  }

  delete[] tmp;
  log("initSA end");
}

void initLcpTree() {
  log("initLcpTree start");
  // Build the LCP tree from the suffix array and inverse suffix array and then
  // delete both
  nodes.push_back(Node());
  TerminalNode.resize(len);

  stack<int> S;
  S.push(0);

  lcp.push_back(0);
  for (int i = 1; i <= len; ++i) {
    if (lcp[i - 1] > lcp[i]) TerminalNode[SA[i - 1]] = S.top();

    int last = -1;
    while (nodes[S.top()].lcp > lcp[i]) {
      if (last != -1) nodes[last].parent = S.top();
      last = S.top();
      S.pop();
    }

    if (nodes[S.top()].lcp < lcp[i]) {
      int idx = nodes.size();
      nodes.push_back(Node());
      nodes[idx].lcp = nodes[idx].len = lcp[i];
      S.push(idx);
    }

    if (last != -1) nodes[last].parent = S.top();
    if (lcp[i - 1] <= lcp[i]) TerminalNode[SA[i - 1]] = S.top();
  }
  lcp.pop_back();

  S.pop();

  lcp.clear();
  SA.clear();
  lcp.shrink_to_fit();
  SA.shrink_to_fit();
  log("initLcpTree end");
}

void calcActivePositions(int x) {
  // Find all active positions for a given node
  int lastActive = 0;
  int cntActive = 0;
  for (uint32_t i = 0; i < nodes[x].positionsCnt; ++i) {
    Position& pos = nodes[x].positions[i];
    if (!TestFlag(pos.flags, Dead) && (lastActive <= pos.pos())) {
      SetFlag(pos.flags, Active);
      lastActive = pos.pos() + nodes[x].len;
      cntActive++;
    } else {
      UnsetFlag(pos.flags, Active);
    }
  }
  nodes[x].setCntActive(cntActive);
}

void initNodePositions() {
  log("initNodePositions start");
  // Get all positions of substring from a node in the LCP tree
  uint64_t positionsCnt = 0;
  for (int i = 0; i < len; ++i) {
    for (int x = TerminalNode[i]; x; x = nodes[x].parent) {
      if (nodes[x].lcp > 1) {
        positionsCnt++;
        nodes[x].positionsCnt++;
      }
    }
  }
  log("len = %d, positionsCnt = %llu, nodes.size() = %u, alpha = %.2lf", len, positionsCnt, nodes.size(), (double)positionsCnt / len);
  invPosStorage.reserve(positionsCnt);

  for (Node& node: nodes) {
    node.positions = new Position[node.positionsCnt];
    node.positionsCnt = 0; // reset temporarily to use for assigning indexes in the second pass
  }

  InversePositions.resize(len);
  for (int i = 0; i < len; ++i) {
    InversePositions[i] = &invPosStorage.back() + 1;
    for (int x = TerminalNode[i]; x; x = nodes[x].parent) {
      if (nodes[x].lcp > 1) {
        invPosStorage.push_back(nodes[x].positionsCnt);
        nodes[x].positions[ nodes[x].positionsCnt++ ] = Position(i);
      }
    }
  }

  for (int i = 0; i < (int)nodes.size(); ++i) calcActivePositions(i);
  log("initNodePositions end");
}

inline void addToPq(int x) {
  // Add a node to the priority queue
  if (!TestFlag(nodes[x].flags, WeightChanged)) return;
  if (nodes[x].cntActive <= 1 || nodes[x].lcp <= 1) return;
  pq[nodes[x].weight()].push_back(x);
  UnsetFlag(nodes[x].flags, WeightChanged);
}

void initPq() {
  log("initPq start");
  // Initialize priority queue
  int maxw = 0;
  for (Node& x : nodes)
    if (x.lcp > 1) maxw = max(maxw, x.weight());
  pq.resize(maxw + 1);
  for (int x = 0; x < (int)nodes.size(); ++x) {
    addToPq(x);
    if (nodes[x].lcp <= 1) SetFlag(nodes[x].flags, Dead);
  }
  log("maxWeight = %d", maxw);
  log("initPq end");
}

vector<pair<int, int>> positionsToFix;

inline void deactivatePosition(int x, int p) {
  if (TestFlag(nodes[x].positions[p].flags, Dead)) return;
  SetFlag(nodes[x].positions[p].flags, Dead);

  if (TestFlag(nodes[x].positions[p].flags, Active)) {
    positionsToFix.emplace_back(x, p);
  }
}

void fixActivePosition(int x, uint32_t p) {
  if (!TestFlag(nodes[x].positions[p].flags, Active)) return;

  UnsetFlag(nodes[x].positions[p].flags, Active);
  uint32_t cntActive = nodes[x].cntActive;
  cntActive--;
  
  uint32_t lastActive = 0;
  while (++p < nodes[x].positionsCnt) {
    if (!TestFlag(nodes[x].positions[p].flags, Dead) &&  lastActive <= nodes[x].positions[p].pos()){  // no overlap
      if (TestFlag(nodes[x].positions[p].flags, Active)) break;
      SetFlag(nodes[x].positions[p].flags, Active);
      cntActive++;
      lastActive = nodes[x].positions[p].pos() + nodes[x].len;
    } else {                                                // overlap or already dead
      if (TestFlag(nodes[x].positions[p].flags, Active)) {  // deactivate
        UnsetFlag(nodes[x].positions[p].flags, Active);
        cntActive--;
      }
    }
  }

  nodes[x].setCntActive(cntActive);
}

int insertNewNode(int x, int nlen, int nlcp) {
  // Insert a node above node x in the LCP tree
  int idx = nodes.size();
  nodes.push_back(Node());
  nodes[idx].len = nlen;
  nodes[idx].lcp = nlcp;
  nodes[idx].parent = nodes[x].parent;
  nodes[idx].positionsCnt = nodes[x].positionsCnt;
  nodes[idx].positions = new Position[ nodes[idx].positionsCnt ];
  ASSERT(nodes[x].positions); 
  memcpy( nodes[idx].positions, nodes[x].positions, nodes[idx].positionsCnt * sizeof(Position) );
  nodes[idx].lastRuleUpdate = nodes[x].lastRuleUpdate;

  if (TestFlag(nodes[x].flags, SamePositionsAsParent))
    SetFlag(nodes[idx].flags, SamePositionsAsParent);

  nodes[x].parent = idx;
  SetFlag(nodes[x].flags, SamePositionsAsParent);

  calcActivePositions(idx);
  addToPq(idx);
  return idx;
}

vector<int> killedByPosIdx;
vector<int> killedSoFar;

void replaceSubstring(const vector<int>& pos, const vector<int>& inv_pos, int ruleLen, int ruleLcp, RuleId ruleIdx, int node) {
  // We replace all substrings of 'ruleLcp' characters starting at positions
  // from list 'pos'.
  // All of those substrings cover 'ruleLen' characters from original string,
  // some of them
  // possibly dead (actually, exactly ruleLen-ruleLcp of them are dead).
  // We call each of those substrings an 'instance'.
  // Here we go through instances from right to left and update the data
  // structures accordingly.
  // For each instance we first kill all the characters (and their positions)
  // but the first one.
  // Those characters are called 'right neighbourhood' of an instance.
  // First character is being replaced by the character denoting the new rule.
  // After that we update characters to the left of the current instance,
  // including the first
  // character of the instance. We limit ourselves to characters which are
  // between current and
  // next instance (i.e. first instance to the left).
  // We call those characters 'left neighbourhood' of the current instance.

  for (int posIdx = (int)pos.size() - 1; posIdx >= 0; --posIdx) {
    // Iterate though all the positions of a new rule
    // Save starting and ending characters
    int a = pos[posIdx];
    int b = pos[posIdx] + ruleLen;
    if (b != len) {
      s[b].prev = a;
    }

    // If appropriate replace output characters with rule number
    if (rules[ruleIdx].len > rules[s[a].ruleId].len) {
      s[a].ruleId = ruleIdx;
    }
    s[a].ruleIds.push_back(ruleIdx);

    // right neighborhood
    int cntKilled = 0;
    int lastAlive = a;
    RuleId sm_rule = longestRuleAtPos(a, ruleLen);
    
    // Iterate through all characters not yet replaced by rules and mark which
    // position of the rule they belong to
    for (int i = a + (sm_rule ? rules[sm_rule].len : 1); i < b; i++) {
      killedByPosIdx[i] = posIdx;
      killedSoFar[i] = ++cntKilled;
      lastAlive = i;
      // If rule already exists skip to end
      if (s[i].ruleId != 0) {
        i += rules[s[i].ruleId].len - 1;
      }
    }
   
    // Iterate backwards through characters in rule position and flag them as Dead
    for (int i = lastAlive; i > a; i = s[i].prev) {
      int x = TerminalNode[i];
      int j = 0;

      while (x) {
        // Use LCP-tree to go through all substrings that start on a given
        // position
        if (posIdx != 0 || i + nodes[x].len > b) {
          // If substring is inside any non-first rule position or starts within
          // a rule and ends outside deactivate it
          if (!TestFlag(nodes[x].flags, Dead))
            deactivatePosition(x, InversePositions[i][j]);
        } 
        if (!TestFlag(nodes[x].flags, SamePositionsAsParent)) j++;
        x = nodes[x].parent;
      }
      // Mark which rule removed a given character
      s[i].killedBy = ruleIdx;
      if (posIdx != 0) {
        // Delete given character
        if (s[i].prev != -1) s[s[i].prev].next = s[i].next;
        if (s[i].next != len) s[s[i].next].prev = s[i].prev;
      }
    }

    int x = TerminalNode[a];
    int j = 0;



    int lo = posIdx == 0 ? 0 : pos[posIdx - 1] + ruleLen;
    // left neighbourhood
    int out=0;
    int k3=-1;
    int i2=-1;

    for (int i = a; i >= lo; i = s[i].prev) {

      int x = TerminalNode[i];
      if (i + nodes[x].len <= a) break;
      int j = 0;
      int k =0;
      int k2=0;
      int j2=0;
      int x2=x;

      if(out!=1)
          while(x2 && i + nodes[x2].len > a ){
              if (TestFlag(nodes[x2].flags, Dead) && (i + nodes[x2].len)>=b && 
                  x2!=node && (nodes[x2].positions[0].pos()==i)) {
                  out=1;
                  i2=i;
                  k3=k2;
              }
              if (!TestFlag(nodes[x2].flags, SamePositionsAsParent)) j2++;
              k2++;
              x2 = nodes[x2].parent;
        }


      while (x && i + nodes[x].len > a) {

        if (!TestFlag(nodes[x].flags, Dead)) {
          /* Here we have two possible cases for a position from the left
             neighbourhood:
                  1.) it intersects with some instance and doesn't contain it
             completely
                  2.) it only contains some instances completely
          */


          if (i + nodes[x].len != len && s[i + nodes[x].len].killedBy == ruleIdx && 
              (i != a || posIdx != 0 || nodes[x].len > ruleLen)) {
              // 1st case
              // Check for a possible new interval
              int idx = killedByPosIdx[i + nodes[x].len];  // ID of the instance
                                                           // we are
                                                           // intersecting.

              int start = pos[idx];  // where the intersecting instance starts?

              // If parent doesn't overlap with the instance, insert a new node
              // in between x and the parent.
              if (i + nodes[nodes[x].parent].len < start){
                // Calculate it's lcp by taking lcp of x and subtracting suffix
                // that's intersecting
                // with the instance.
                int newLcp = nodes[x].lcp - killedSoFar[i + nodes[x].len];
                if (!TestFlag(nodes[x].positions[InversePositions[i][j]].flags,Dead)) {
                  int idx = insertNewNode(x, start - i, newLcp);
                }
              }
                
              // Kill the position because it's no longer valid
              if (!TestFlag(nodes[x].positions[InversePositions[i][j]].flags, Dead)) {
                deactivatePosition(x, InversePositions[i][j]);
              }
            } else {
              // 2nd case
              if ((i != a || posIdx != 0 || nodes[x].len > ruleLen) &&  
                (out==0 || (i==i2 && k>k3)) ) {
                // Rules that were left active inside the first position of a
                // rule shouldn't affect anything outside of it
                    
                // If this position is alive, subtract instances completely
                // inside the substring,
                // but only if that hasn't already been done for that interval.
                if (!TestFlag(nodes[x].positions[InversePositions[i][j]].flags, Dead) &&
                    nodes[x].lastRuleUpdate != ruleIdx) {

                  int cnt = 0;
                  if (unlikely(posIdx + 8 > pos.size())) {
                    cnt = upper_bound(pos.begin() + posIdx, pos.end(), i + nodes[x].len - ruleLen) - pos.begin() - posIdx;
                  } else {
                    while (posIdx + cnt < pos.size() && pos[posIdx + cnt] <= i + nodes[x].len - ruleLen) cnt++;
                  }
                  nodes[x].setLcp(nodes[x].lcp - cnt * (ruleLcp - 1));
                  nodes[x].lastRuleUpdate = ruleIdx;
                  addToPq(x);
                }
              } 
            }
        }

        if (!TestFlag(nodes[x].flags, SamePositionsAsParent)) j++;
        k++;
        x = nodes[x].parent;
      }
    }
  }
  // First we marked as dead all the positions that need to be deactivated.
  // Now we go through all of them and deactivate them.
  for (int i = (int)positionsToFix.size() - 1; i >= 0; --i)
    fixActivePosition(positionsToFix[i].first, positionsToFix[i].second);
  for (auto& p : positionsToFix) addToPq(p.first);
  positionsToFix.clear();
}

string getSubstring(int a, int b) {
  string ret = "";
  for (int i = a; i < b; i++) {
    ret.push_back((char)s[i].ch);
  }
  return ret;
}

string reconstruct(int start, int length) {
  // reconstruct the original string from the created rules
  string result = "";
  int end = start + length;
  int start1 = start;
  RuleId sm_rule = longestRuleAtPos(start, length);

  if (sm_rule != 0) {
    result += reconstruct(rules[sm_rule].first, rules[sm_rule].len);
    start1 = start + rules[sm_rule].len;
  } else {
    result += s[start].ch;
    start1++;
  }
  for (int i = start1; i < end; i++) {
    if (s[i].ruleId == 0) {
      result += s[i].ch;
    } else {
      result += reconstruct(rules[s[i].ruleId].first, rules[s[i].ruleId].len);
      i += rules[s[i].ruleId].len - 1;
    }
  }
  return result;
}

void cleanup() {
  killedByPosIdx.clear();
  killedSoFar.clear();
  for (Node& node: nodes) {
    delete[] node.positions;
  }
  nodes.clear();

  SA.clear();
  lcp.clear();
  TerminalNode.clear();

  pq.clear();

  invPosStorage.clear();
  InversePositions.clear();

  text.clear();
  len = 0;

  s.clear();

  rules.clear();
  pravila.clear();
}

void runMainAlgorithm() {
  log("runMainAlgorithm1 start");
  // Main function, iterates through substrings by weight, creates rules and
  // updates all other structures accordingly
  killedByPosIdx.resize(len + 1);
  killedSoFar.resize(len + 1);

  // fake rule 0
  rules.emplace_back(0, 0);
  
  vector<int> v;
  vector<int> inv_pos;
  int k=0;
  for (int w = (int)pq.size() - 1; w >= 0; --w) {
    // In order of decreasing weight iterate through substrings
    while (pq[w].size()) {
      // Select a substring with maximum weight,flag it as Dead and remove from
      // PQ
      int i = pq[w].back();
      pq[w].pop_back();
      
      // Check if this is the valid entry for the node i
      if (w != nodes[i].weight() || TestFlag(nodes[i].flags, Dead)) continue;
      

      SetFlag(nodes[i].flags, Dead);
      // Get the length and all positions of the substring
      int len = nodes[i].len;
      int lcp = nodes[i].lcp;
      v.clear();
      inv_pos.clear();
      for (uint32_t j = 0; j < nodes[i].positionsCnt; j++) {
        if (TestFlag(nodes[i].positions[j].flags, Active)) {
          inv_pos.push_back(j);
          v.push_back(nodes[i].positions[j].pos());
        }
      }
      // Create a new rule, mark it's first location and increase rule counter
      RuleId ruleId = rules.size();
      rules.emplace_back(len, v[0]);

      // Call update function
      replaceSubstring(v,inv_pos, len, lcp, ruleId, i);

      // Free up memory
      //Keep first position saved for checking if something is alive inside it
      delete[] nodes[i].positions;
      nodes[i].positions= new Position[1];
      nodes[i].positions[0] = Position (v[0]);
      SetFlag(nodes[i].positions[0].flags,Dead);
      nodes[i].positionsCnt = 1;
    }
    pq[w].shrink_to_fit();
  }
  log("runMainAlgorithm end");

}

} // namespace

vector<vector<int>> laf(const std::string& input) {
  cleanup();

  // Convert the input into the appropriate representation
  text = input;
  len = text.size();
  s.resize(len);
  for (int i = 0; i < len; ++i) s[i] = {text[i], i - 1, i + 1, 0, 0, {}};

  // Run all initialisations
  initSA();
  initLcpTree();
  initNodePositions();
  initPq();
  // Run the LAF compression algorithm
  runMainAlgorithm();

  // Convert the output into the appropriate representation
  vector<vector<int>> output;
  //   First extract the compressed string 
  {
    vector<int> textOutput;
    for (int j = 0; j < len; j++) {
      if (s[j].ruleId > 0) {
        textOutput.push_back(TERMINAL_OFFSET + s[j].ruleId);
        j += rules[s[j].ruleId].len - 1;
      } else {
        textOutput.push_back(int(s[j].ch));
      }
    }
    output.push_back(textOutput);
  }

  //   Finally, extract the definition of rules.
  for (uint32_t i = 1; i < rules.size(); i++) {
    vector<int> ruleOutput;

    int j = rules[i].first;
    RuleId sm_rule = longestRuleAtPos(rules[i].first, rules[i].len);
    if (sm_rule > 0) {
      ruleOutput.push_back(TERMINAL_OFFSET + sm_rule);
      j += rules[sm_rule].len;
    } else {
      ruleOutput.push_back(int(s[j].ch));
      j++;
    }
    for (; j < rules[i].first + rules[i].len; j++) {
      if (s[j].ruleId > 0) {
        ruleOutput.push_back(TERMINAL_OFFSET + s[j].ruleId);
        j += rules[s[j].ruleId].len - 1;
      } else {
        ruleOutput.push_back(int(s[j].ch));
      }
    }
    output.push_back(ruleOutput);
  }

  return output;
}
