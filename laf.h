#include <string>
#include <vector>

const int TERMINAL_OFFSET = 256;

// Takes a string and performs LAF compression on it.
// Returns a sequence (std::vector) of strings over integer 
// alphabet (to accomodate for non-terminals).
// In particular, terminal characters have their ordinary ASCII values, while 
// Rule "i" (1-based) is represented as an integer 256+i.

// String at position 0 is the compressed version of the input string.
// Remaining strings represent definition of rules.
// Definition of rule "i" is at position i in the returned sequence.

std::vector<std::vector<int>> laf(const std::string& s);