# ESA LAF

ESA LAF is a C++11 implementation of the fast algorithm for the largest 
area first parsing of a string - preprocessing for grammar compression.

The algorithm is described in:

Ivan Katanić, Strahil Ristov, Martin Rosenzweig
A Fast Algorithm for the Largest Area First Parsing of Real Strings.
IEEE Access 8 (2020, 141990-142002)

# Source code
The source code consists of:

`laf.h`/`laf.cpp` that exposes a single function that performs the LAF parsing on a given string,

`main.cpp` that runs the algorithm using `laf.h` on a given text file and stores the compressed representation in the given output file and

`test.cpp` that performs an optimality test on the output of laf.cpp by comparing it to a brute force method 
  
# Building
You need `g++` to build this project. 

To build the project run:
```
cd /path/to/this/project
make
```

# Running 
`./main /path/to/input /path/to/output`

Output will consist of rules in order of their corresponding area sizes, and
the grammar compressed representation of the input file.

# Testing
Provided with this project is an optimality tester.

For example, to run the tester over random strings of length 20 and alphabet `{a,b,c}`
run:
```
./test 20 3
```

# Note
`sais.cpp` is a source code for an efficient suffix array construction by
Ge Nong, Sen Zhang and Wai Hong Chan, described in paper "Two efficient
algorithms for linear time suffix array construction" IEEE Trans.
Computers 60(10) (2011), originally downloaded from
https://code.google.com/archive/p/ge-nong/downloads. Thanks!

