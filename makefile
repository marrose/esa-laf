all: main test

main: main.cpp laf.cpp laf.h
	g++ --std=c++11 -O3 main.cpp laf.cpp -o main

test: test.cpp laf.cpp laf.h
	g++ --std=c++11 -O3 test.cpp laf.cpp -o test

clean:
	rm main test
